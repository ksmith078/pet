#VIRTUAL PETS
#import requests
import time
global name
global breed
global sex
global pronoun
global age
global temperature
global dead
global pronounSet
temperature = 37.4
dead = False
name =""
sex = ""
#a list of sets of pronouns which can be added into a sentence at any time
pronouns = [["he","him","his","is he", 'he is','isnt he','he doesnt'],
            ["she", 'her', 'her', 'is she', 'she is', "isnt she", "she doesnt"],
            ['they','them', 'their', 'are they', 'they are', 'arent they', "they dont"]]
#a variable which control which set of pronouns the pet uses
pronounSet = 0
age = 0

actions = ['Pet', 'Feed', 'Stroke', 'Go on a walk with', 'Play with', 'Pick up poo left by', 'Bath']
#the valid inputs for any yes/no question
yNValid = [["yes", "Yes", "y", "Y"],["no","No","n","N"]]
def pet():
    print("pet")

def feed():
    print("feed")
def stroke():
    print("stroke")
def walk():
    print("walk")
def pickUpPoo():
    print("pickUpPoo")
#the statistics the dog has at any one time
wellBeing = {
    "hunger": 50,
    "happy": 50,
    "thirst": 50,
    "clean":50,
}
def walk():
    walkPrompt = name + " is looking restless, would you like to take "+pronouns[pronounSet][1]+" for a walk?"
    walkCheck = input(walkPrompt)
#check that the input is recognised as yes or no
    while walkCheck not in yNValid[0] and walkCheck not in yNValid[1]:
        walkCheck = input("Please answer yes or no")
    if walkCheck in yNValid[0]:
        print("How far would you like to take", pronouns[pronounSet][1],"?")
        print("Will it be: \n 1 A short walk \n 2 The usual route \n 3 A long walk")
        walkChoice = input("Choose a number: ")
        valid = ('1', '2', '3')
        while walkChoice not in valid:
            walkChoice = input("Please choose either 1, 2 or 3: ")



def bath():
# a number which relates to a certain descriptive phrase about the pets cleanliness
    adjNum= 1
#descriptive phrases about the pets cleanliness
    adj = ["","squeaky clean", "a bit musty", "in need of a wash", "really starting to smell", "absolutely disgusting", "at risk of disease"]
#constructing a compound sentence including pronouns for when the dog is the cleanest it can be
    adj[0] = "looking " + pronouns[pronounSet][2] + " very best"
#allocating a descriptive phrase based on how high the pets clean rating is
    if wellBeing["clean"] > 100 or wellBeing["clean"]==100:
        adjNum = 0

    elif wellBeing["clean"] < 100 and wellBeing["clean"] > 51:
        adjNum = 1
    elif wellBeing["clean"]< 50 and wellBeing["clean"] > 41 or wellBeing["clean"]==50:
        adjNum = 2
    elif wellBeing["clean"]<40 and wellBeing["clean"]>31 or wellBeing["clean"]==40:
        adjNum = 3
    elif wellBeing["clean"]<30 and wellBeing["clean"]>21 or wellBeing["clean"]==30:
        adjNum = 4
    elif wellBeing["clean"]<20 and wellBeing["clean"]>11 or wellBeing["clean"]==20:
        adjNum = 5
    else:
        adjNum = 6
#ask if the user would like to bathe the pet
    print("Your dog is", adj[adjNum])
    print("Would you like to bathe", pronouns[pronounSet][1])
    bathCheck =input("Yes/No :")
#check whether the input is a valid yes or no answer
    while bathCheck not in yNValid[0] and bathCheck not in yNValid[1]:
        bathCheck = ("Please answer yes or no")
    if bathCheck in yNValid[0]:
        wellBeing["clean"] = 80
        adjNum = 1
        print("SCRUBADUB-DUB! Your pooch is now",adj[adjNum])
        groomPrompt ="Would you like to go the extra mile and give "+pronouns[pronounSet][1]+" a fancy grooming?"
        groomCheck = input(groomPrompt)
#check whether the input is a valid yes or no answer
        while groomCheck not in yNValid[0] and groomCheck not in yNValid[1]:
            groomCheck = ("Please answer yes or no")
#If the user decides to groom the pet, the cleanliness is set to 100 and the happiness increases by 20
        if groomCheck in yNValid[0]:
            wellBeing["clean"] = 100
            adjNum = 0
            wellBeing["happy"] = wellBeing["happy"] + 20
            print("My, my, ",pronouns[pronounSet][5], "just", adj[adjNum])
#if the user decides not to groom the pet, the dog pressures them to go on a walk
        elif groomCheck in yNValid[1]:
            print("Oh dear",pronouns[pronounSet][6],"look very happy about that!" )
            print("'WOOF! WOOF!'")
            print("*Grabs",pronouns[pronounSet][2],"lead and walks towards the door*")
            walk()
    elif bathCheck in yNValid[1]:
#prints a message if the user decides not to bathe the pet
        print("Looks like",pronouns[pronounSet][4], "just going to have to stay", adj[adjNum], "for a while longer")


def play():
    if wellBeing["happy"] < 70:
        print("Your pet looks bored. Want to play?")
        userinput = input("Play?: ")
        if userinput == 'yes':
            print(name, "runs back and forth")
            wellBeing["happy"] = wellBeing["happy"] + 10
            wellBeing["hunger"] = wellBeing["hunger"] - 10
            if wellBeing["hunger"]  < 60:
                feed()
        elif userinput == 'no':
            print(name, "barks angrily and runs off")
            wellBeing["happy"] = wellBeing["happy"] - 10
            wellBeing["clean"] = wellBeing["clean"] - 10
            if wellBeing["clean"] < 50:
                bath()
                pass
        else:
            print("invalid")
        if wellBeing["happy"] > 80:
            print(name, "is very happy now")
        elif wellBeing["happy"] < 50:
            print("your pet could be happier!")
        elif wellBeing["happy"] < 20:
            print(name, "seems quite unhappy")


def feed():
    if wellBeing["hunger"] == 100:
        print("Your pet is full and can't eat anymore!")
    elif wellBeing["hunger"]  < 100:
        print("Your pet is hungry, want to feed him?")
        input("Feed pet?: ")
        if input == 'yes':
            wellBeing["hunger"]  = wellBeing["hunger"]  + 10
            print("You fed your pt. His hunger is now", wellBeing["hunger"] )
            thirst = thirst - 10
            print("hunger is now", wellBeing["hunger"] ,"%")
        elif input == 'no':
            print("Fine let him starve")
        else:
            feed()
    else:
        print("error")


def menu():
#a list of valid values for userinput
    valid = ('1', '2', '3', '4' ,'5', '6', '7')
    print("What would you like to do with your dog now?")
    print("--------PLEASE CHOOSE FROM THE OPTIONS BELOW--------")
    num = 1
#iterate for the length of the actions array and print a numbered list of all the possible actions
    for num, i in enumerate(actions):
        print (num+1,i, pronouns[pronounSet][1])

    choice = input("Enter the number of your choice: ")
#if the input matches with on of the valid numbers it is accepted
    if choice in valid:
        print("INPUT ACCEPTED")
#the user is prompted for an input until the one given is valid
    while choice not in valid:
        choice = input("INVALID INPUT \nPlease enter a valid number: ")
#calls a different function for each one of the options
    while choice in valid:
        if choice == "1":
            pet()
            break
        elif choice == "2":
            feed()
            break
        elif choice == '3':
            stroke()
            break
        elif choice == '4':
            walk()
            break
        elif choice == '5':
            play()
            break
        elif choice == '6':
            pickUpPoo()
            break
        else:
            bath()
            break


# This function gets the details of the dog the user wants
def getDeets():
    print("Congrats! You've just bought a new dog. Want to name them?")
    name = input("Name of dog: ")
    print("Wow,",name,"is such a cute name!" )
    breed = input("What breed are they?: ")
    sex = input("Lovely!, Boy or girl?: ")
    if sex == 'boy':
        pronounSet = 0
    elif sex == 'girl':
        pronounSet = 1
    else:
        pronounSet = 2
    print("Wow, a", sex, breed,"called", name,"!")
    howOldInput = "How old " + pronouns[pronounSet][3] + ": "
    age = input(howOldInput)
    age = int(age)
    if age > 2:
        print("Aw a", age, "year old", breed, "so cute")
    print("You need to look after your pet by taking care of their needs.")
    input("Press enter ")
    menu()



def drink():
    wellBeing["thirst"] = wellBeing["thirst"] + 10
    loop = True
    while loop == True:
        if wellBeing["thirst"] < 50:
            print(name, "Looks very thirsty!")
            drinkInput = "Give " + name + " another drink?: "
            userinput = input(drinkInput)
            if userinput == 'yes':
                wellBeing["thirst"] = wellBeing["thirst"] + 10
                if wellBeing["thirst"] > 100:
                    print(name, " stops drinking and walks away")
                    wellBeing["happy"] = wellBeing["happy"] + 10
                    if wellBeing["happy"] >= 90:
                        print(name, " looks happy!")
                    else:
                        print(name, "could be happier")
            elif userinput == 'no':
                 print(name, "looks sad")
                 wellBeing["happy"] = wellBeing["happy"] - 10
                 loop = False
walk()
#getDeets()
#menu()
